package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"
)

/*

	a proxy based on cookie


	usage:

		proxy -i "grafana,http://localhost:3000" -i "collector,http://localhost:8080"


	select a site
		GET /Proxy/<name>   eg GET /Proxy/collector

		==> set cookie to this site and redirect to it
			all subsequent traffic will be redirected to this site as the long the cookie is valid


*/

// Define a type named "intslice" as a slice of ints
type entries map[string]string

// Now, for our new type, implement the two methods of
// the flag.Value interface...
// The first method is String() string
func (e entries) String() string {
	s := ""
	for k, v := range e {
		s += fmt.Sprintf("%s : %s\n", k, v)
	}
	return s
}

// The second method is Set(value string) error
func (e entries) Set(value string) error {

	// value is a commas delimited string  proxyName,proxyURL eg grafana,http://localhost:3000
	fmt.Printf("%s\n", value)
	parts := strings.Split(value, ",")
	if len(parts) != 2 {
		return errors.New(fmt.Sprintf("Invalid proxy argument:%s", value))
	}
	e[parts[0]] = parts[1]

	return nil
}

// Serve a reverse proxy for a given url
func serveReverseProxy(target string, res http.ResponseWriter, req *http.Request) {
	// parse the url
	url, _ := url.Parse(target)

	// create the reverse proxy
	proxy := httputil.NewSingleHostReverseProxy(url)

	/*
		// Update the headers to allow for SSL redirection
		req.URL.Host = url.Host
		req.URL.Scheme = url.Scheme
		req.URL.Path = url.Path
		req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
		req.Host = url.Host

	*/

	// Note that ServeHttp is non blocking and uses a go routine under the hood
	proxy.ServeHTTP(res, req)
}

func redirect(w http.ResponseWriter, req *http.Request) {
	// redirect to proxy root
	target := "http://" + req.Host + "/"
	if len(req.URL.RawQuery) > 0 {
		target += "?" + req.URL.RawQuery
	}
	log.Printf("redirect to: %s", target)
	http.Redirect(w, req, target,
		// see comments below and consider the codes 308, 302, or 301
		http.StatusTemporaryRedirect)
}

func SetCookieProxy(w http.ResponseWriter, value string) {
	expire := time.Now().Add(20 * time.Minute) // Expires in 20 minutes
	cookie := http.Cookie{Name: "CookieProxy", Value: value, Path: "/", Expires: expire, MaxAge: 86400}
	http.SetCookie(w, &cookie)
}

func setAndRedirect(name string, w http.ResponseWriter, r *http.Request) {
	SetCookieProxy(w, name)
	// redirect to proxy with cookie set
	redirect(w, r)
}

type Proxy struct {
	Entries entries
}

func NewProxy(entries entries) *Proxy {

	p := new(Proxy)
	p.Entries = entries
	p.SetRoutes()

	return p
}

func (p *Proxy) SetRoutes() {

	http.HandleFunc("/Proxy/", func(w http.ResponseWriter, r *http.Request) {
		// redirect to proxy with cookie set
		parts := strings.Split(r.URL.Path, "/")
		if len(parts) == 3 {
			proxyName := parts[2]
			if proxyName != "" {
				_, ok := p.Entries[proxyName]
				if ok == true {
					setAndRedirect(proxyName, w, r)
					return
				}
			} else {
				// /Proxy/ display proxies
				fmt.Fprintf(w, "<body>%s</body>", p.Entries.String())
				//w.Write([]byte(proxies.String()))
				return
			}
		}
		// proxy not found
		http.Error(w, "404 not found.", http.StatusNotFound)
		//w.Write([]byte("proxy not found"))
		return
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		// check if cookie CookieProxy is present
		var proxyName string

		cookie, err := r.Cookie("CookieProxy")
		if err != nil {
			proxyName = ""
		} else {
			proxyName = cookie.Value
		}
		// find proxy target
		proxyToAddr, ok := p.Entries[proxyName]
		if ok == false {
			// not found
			// reset cookie
			http.SetCookie(w, &http.Cookie{Name: "CookieProxy", Value: ""})
			http.Error(w, "404 not found.", http.StatusNotFound)
			return
		}

		serveReverseProxy(proxyToAddr, w, r)
		return

	})

}

func (p *Proxy) Run(ctx context.Context, addr string) {

	if addr == "" {
		addr = ":1323"
	}
	// start server
	if err := http.ListenAndServe(addr, nil); err != nil {
		panic(err)
	}
}

func main() {

	proxies := make(entries)
	//var defaultProxy string

	flag.Var(&proxies, "i", "add a site to proxy  eg 'grafana,http://localhost:3000' ")
	//flag.String(defaultProxy, "default", "default site to proxy")

	flag.Parse()
	if flag.NFlag() == 0 {
		flag.PrintDefaults()
	} else {
		fmt.Println("Here are the values in 'entries'")
		for k, v := range proxies {
			fmt.Printf("%s : %s ", k, v)
		}
	}

	//proxy := NewProxy()

	http.HandleFunc("/Proxy/", func(w http.ResponseWriter, r *http.Request) {
		// redirect to proxy with cookie set
		parts := strings.Split(r.URL.Path, "/")
		if len(parts) == 3 {
			proxyName := parts[2]
			if proxyName != "" {
				_, ok := proxies[proxyName]
				if ok == true {
					setAndRedirect(proxyName, w, r)
					return
				}
			} else {
				// /Proxy/ display proxies
				fmt.Fprintf(w, "<body>%s</body>", proxies.String())
				//w.Write([]byte(proxies.String()))
				return
			}
		}
		// proxy not found
		http.Error(w, "404 not found.", http.StatusNotFound)
		//w.Write([]byte("proxy not found"))
		return
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		// check if cookie CookieProxy is present
		var proxyName string

		cookie, err := r.Cookie("CookieProxy")
		if err != nil {
			proxyName = ""
		} else {
			proxyName = cookie.Value
		}
		// find proxy target
		proxyToAddr, ok := proxies[proxyName]
		if ok == false {
			// not found
			// reset cookie
			http.SetCookie(w, &http.Cookie{Name: "CookieProxy", Value: ""})
			http.Error(w, "404 not found.", http.StatusNotFound)
			return
		}

		serveReverseProxy(proxyToAddr, w, r)
		return

	})

	// start server
	if err := http.ListenAndServe(":1323", nil); err != nil {
		panic(err)
	}

}
