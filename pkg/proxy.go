package proxy

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"
)

/*

	a proxy based on cookie


	usage:

		proxy -i "grafana,http://localhost:3000" -i "collector,http://localhost:8080"


	select a site
		GET /Proxy/<name>   eg GET /Proxy/collector

		==> set cookie to this site and redirect to it
			all subsequent traffic will be redirected to this site as the long the cookie is valid


*/

// Define a type named "intslice" as a slice of ints
type Entries map[string]string

// Now, for our new type, implement the two methods of
// the flag.Value interface...
// The first method is String() string
func (e Entries) String() string {
	s := ""
	for k, v := range e {
		s += fmt.Sprintf("%s : %s\n", k, v)
	}
	return s
}

// The second method is Set(value string) error
func (e Entries) Set(value string) error {

	// value is a commas delimited string  proxyName,proxyURL eg grafana,http://localhost:3000
	fmt.Printf("%s\n", value)
	parts := strings.Split(value, ",")
	if len(parts) != 2 {
		return errors.New(fmt.Sprintf("Invalid proxy argument:%s", value))
	}
	e[parts[0]] = parts[1]

	return nil
}

// Serve a reverse proxy for a given url
func serveReverseProxy(target string, res http.ResponseWriter, req *http.Request) {
	// parse the url
	url, _ := url.Parse(target)

	// create the reverse proxy
	proxy := httputil.NewSingleHostReverseProxy(url)

	/*
		// Update the headers to allow for SSL redirection
		req.URL.Host = url.Host
		req.URL.Scheme = url.Scheme
		req.URL.Path = url.Path
		req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
		req.Host = url.Host

	*/

	// Note that ServeHttp is non blocking and uses a go routine under the hood
	proxy.ServeHTTP(res, req)
}

func redirect(w http.ResponseWriter, req *http.Request) {
	// redirect to proxy root
	target := "http://" + req.Host + "/"
	if len(req.URL.RawQuery) > 0 {
		target += "?" + req.URL.RawQuery
	}
	log.Printf("redirect to: %s", target)
	http.Redirect(w, req, target,
		// see comments below and consider the codes 308, 302, or 301
		http.StatusTemporaryRedirect)
}

type Proxy struct {
	Entries    Entries
	CookieName string        // CookieProxy
	Expires    time.Duration // 20 * time.Minute)
	MaxAge     int           // 86400
}

func NewProxy(entries Entries) (proxy *Proxy, err error) {

	p := new(Proxy)
	p.Entries = entries
	p.CookieName = "CookieProxy"
	p.MaxAge = 86400
	p.SetRoutes()
	p.Expires, err = time.ParseDuration("120s")

	return
}

func (p *Proxy) SetRoutes() {

	http.HandleFunc("/Proxy/", func(w http.ResponseWriter, r *http.Request) {
		// redirect to proxy with cookie set
		parts := strings.Split(r.URL.Path, "/")

		// if len(parts) == 2 {
		// 	//  /Proxy   => display list of sites
		// 	RenderChoice(p.Entries, w, r)
		// 	return
		// }

		if len(parts) == 3 {
			// /Proxy/$name
			proxyName := parts[2]
			if proxyName != "" {
				_, ok := p.Entries[proxyName]
				if ok == true {
					p.setAndRedirect(proxyName, w, r)
					return
				}
			} else {
				// /Proxy/ display proxies
				RenderChoice(p.Entries, w, r)
				//fmt.Fprintf(w, "<body>%s</body>", p.Entries.String())
				//w.Write([]byte(proxies.String()))
				return
			}
		}
		// proxy not found
		http.Error(w, "404 not found.", http.StatusNotFound)
		//w.Write([]byte("proxy not found"))
		return
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		// check if cookie CookieProxy is present
		var proxyName string

		cookie, err := r.Cookie(p.CookieName)
		if err != nil {
			// no cookie -> redirect to /Proxy
			proxyName = ""
			http.Redirect(w, r, "/Proxy",
				// see comments below and consider the codes 308, 302, or 301
				http.StatusTemporaryRedirect)
			return
		} else {
			proxyName = cookie.Value
		}
		// find proxy target
		proxyToAddr, ok := p.Entries[proxyName]
		if ok == false {
			// site not found
			// reset cookie
			http.SetCookie(w, &http.Cookie{Name: p.CookieName, Value: ""})
			http.Error(w, "404 not found.", http.StatusNotFound)
			return
		}

		serveReverseProxy(proxyToAddr, w, r)
		return

	})

}

func (p *Proxy) SetCookieProxy(w http.ResponseWriter, value string) {
	expire := time.Now().Add(p.Expires) // 20 * time.Minute) // Expires in 20 minutes
	cookie := http.Cookie{Name: p.CookieName, Value: value, Path: "/", Expires: expire, MaxAge: p.MaxAge}
	http.SetCookie(w, &cookie)
}

func (p *Proxy) setAndRedirect(name string, w http.ResponseWriter, r *http.Request) {
	p.SetCookieProxy(w, name)
	// redirect to proxy with cookie set
	redirect(w, r)
}

func (p *Proxy) Run(ctx context.Context, addr string) {

	if addr == "" {
		addr = ":1323"
	}
	// start server
	if err := http.ListenAndServe(addr, nil); err != nil {
		panic(err)
	}
}
