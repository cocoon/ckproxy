package proxy

import (
	"html/template"
	"log"
	"net/http"
)

/*
	display site choice
	<h2> Proxy choices</h2>
	<ul>
		<li><a href="/Proxy/collector">collector</a> </li>
		<li><a href="/Proxy/grafana">grafana</a> </li>
	</ul>

*/

var choiceTemplate = `
<!DOCTYPE html>
<html>
  <head>
  <title>Proxy</title>
  <style>
  @import "compass/css3";

  div {
	width: 200px;
	margin: 30px;
  }
  
  h2 {
	font: 400 40px/1.5 Helvetica, Verdana, sans-serif;
	margin: 0;
	padding: 0;
  }
  
  ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
  }
  
  li {
	font: 200 20px/1.5 Helvetica, Verdana, sans-serif;
	border-bottom: 1px solid #ccc;
  }
  
  li:last-child {
	border: none;
  }
  
  li a {
	text-decoration: none;
	color: #000;
  
	-webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
	-moz-transition: font-size 0.3s ease, background-color 0.3s ease;
	-o-transition: font-size 0.3s ease, background-color 0.3s ease;
	-ms-transition: font-size 0.3s ease, background-color 0.3s ease;
	transition: font-size 0.3s ease, background-color 0.3s ease;
	display: block;
	width: 200px;
  }
  
  li a:hover {
	font-size: 30px;
	background: #f6f6f6;
  }
  
  </style>
  </head>
  <body>
  	<div>
		<h2>Proxy</h2>
		<ul>
			{{range $key, $value := .}}
			<li><a href="/Proxy/{{$key}}">{{$key}}</a> </li>
			{{end}}
		</ul>
		</div>
  </body>
</html>
`

func RenderChoice(entries Entries, w http.ResponseWriter, r *http.Request) {

	t, err := template.New("choices").Parse(choiceTemplate)
	if err == nil {
		err = t.Execute(w, entries)
		if err != nil {
			log.Printf("%s\n", err.Error())
		}
		return
	}
	log.Printf("%s\n", err.Error())
	return
}
