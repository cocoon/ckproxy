package proxy

import (
	"context"
	"log"
	"testing"
)

func TestProxy(t *testing.T) {

	entries := Entries{
		"collector":    "http://localhost:8080",
		"grafana":      "http://localhost:3000",
		"alertmanager": "http://localhost:9093",
		"prometheus":   "http://localhost:9090",
		"watchers":     "http://localhost:9001",
	}

	ckp, err := NewProxy(entries)
	if err != nil {
		log.Printf("%s\n", err.Error())
		return
	}
	ctx := context.Background()
	addr := ":1323"
	ckp.Run(ctx, addr)

}
