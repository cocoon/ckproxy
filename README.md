
# cookie proxy


a reverse proxy base on a cookie CookieProxy


When a request arrive

if no cookie exists
    check path  ( collector grafana prometheus alertmanager watcher )

    redirect / with cookie set to path


if cookie exists

    create reverse proxy for the request to target indicated by cookie

