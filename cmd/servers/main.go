package main

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
)

func Server(name string) *echo.Echo {

	s := echo.New()
	s.Use(middleware.Logger())
	s.Use(middleware.Recover())

	s.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, name)
	})

	s.GET("/static", func(c echo.Context) error {
		return c.String(http.StatusOK, fmt.Sprintf("%s/static", name))
	})

	return s
}

func main() {

	s1 := Server("collector")
	go s1.Start(":8080")

	s2 := Server("grafana")
	go s2.Start(":3000")

	select {}

}
