package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	proxy "bitbucket.org/cocoon/ckproxy/pkg"
)

func main() {

	entries := make(proxy.Entries)
	//var defaultProxy string

	flag.Var(&entries, "i", "add a site to proxy  eg 'grafana,http://localhost:3000' ")
	//flag.String(defaultProxy, "default", "default site to proxy")

	flag.Parse()
	if flag.NFlag() == 0 {
		flag.PrintDefaults()
	} else {
		fmt.Println("Here are the values in 'entries'")
		for k, v := range entries {
			fmt.Printf("%s : %s ", k, v)
		}
	}

	ckp, err := proxy.NewProxy(entries)
	if err != nil {
		log.Printf("%s\n", err.Error())
		return
	}
	ctx := context.Background()
	addr := ":1323"
	ckp.Run(ctx, addr)

}
